"""
class Student:
    name:str
    age:int
    reg_no: str
"""


from database import BASE
from sqlalchemy import Column,String,Integer,Text


class Student(BASE):
    __tablename__= 'students'
    id = Column(Integer(),primary_key=True)
    name = Column(String())
    age = Column(Integer())
    registration_no = Column(Text())


    def __repr__(self):
        return f"<Student {self.name}>"